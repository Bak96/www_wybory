from jinja2 import Environment, PackageLoader
from jinja2 import select_autoescape
import json
import copy

f_glosy_niewazne = 'glosy_niewazne'
f_glosy_oddane = 'glosy_oddane'
f_glosy_wazne = 'glosy_wazne'
f_gmina = 'gmina'
f_kandydaci = 'kandydaci'
f_karty_wydane = 'karty_wydane'
f_nazwa_gminy = 'nazwa_gminy'
f_obwody = 'obwody'
f_powiat = 'powiat'
f_okreg = 'okreg'
f_uprawnieni = 'uprawnieni'
f_wojewodztwo = 'wojewodztwo'

def stworz_pusty_rekord(zrodlo):
    rekord = {}
    rekord[f_glosy_wazne] = 0
    rekord[f_glosy_niewazne] = 0
    rekord[f_glosy_oddane] = 0
    rekord[f_kandydaci] = {}
    rekord[f_karty_wydane] = 0
    rekord[f_obwody] = 0
    rekord[f_uprawnieni] = 0
    for key in zrodlo[f_kandydaci]:
        rekord[f_kandydaci][key] += zrodlo[f_kandydaci][key]
    return

def dodaj_do_sumy(cel, zrodlo):
    cel[f_glosy_niewazne] += zrodlo[f_glosy_niewazne]
    cel[f_glosy_oddane] += zrodlo[f_glosy_oddane]
    cel[f_glosy_wazne] += zrodlo[f_glosy_wazne]
    cel[f_karty_wydane] += zrodlo[f_karty_wydane]
    cel[f_obwody] += zrodlo[f_obwody]
    cel[f_uprawnieni] += zrodlo[f_uprawnieni]

    for key in zrodlo[f_kandydaci]:
        cel[f_kandydaci][key] += zrodlo[f_kandydaci][key]
    return

from pprint import pprint
env = Environment(
    loader=PackageLoader('generator', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)
print('hello')

with open('polaczone.json') as data_file:
    data = json.load(data_file)

#pprint(data)

lacznaSumaGlosow = 0

kandydaci = {}
wojewodztwa = {}
okregi = {}
gminy = {}
wszystko = {}


mydictionary = data["gminy"][0]["kandydaci"]
for key in mydictionary:
    #print("key: %s , value: %s" % (key, mydictionary[key]))
    kandydaci[key] = 0

for rekord in data["gminy"]:
    if not wszystko:
        wszystko = copy.deepcopy(rekord)
    else:
        dodaj_do_sumy(wszystko, rekord)

    for kandydat in rekord["kandydaci"]:
        kandydaci[kandydat] += rekord["kandydaci"][kandydat]
    lacznaSumaGlosow += rekord[f_glosy_wazne]
    #sumowanie dla wojewodztw

    if rekord[f_wojewodztwo] in wojewodztwa:
        dodaj_do_sumy(wojewodztwa[rekord[f_wojewodztwo]], rekord)
    else:
        wojewodztwa[rekord[f_wojewodztwo]] = copy.deepcopy(rekord)
        wojewodztwa[rekord[f_wojewodztwo]]['listaOkregow'] = set()

    #dodaje wojewodztwu kolejny okreg
    wojewodztwa[rekord[f_wojewodztwo]]['listaOkregow'].add(rekord[f_okreg])

    #sumowanie dla okregow
    if rekord[f_okreg] in okregi:
        dodaj_do_sumy(okregi[rekord[f_okreg]], rekord)
    else:
        okregi[rekord[f_okreg]] = copy.deepcopy(rekord)
        okregi[rekord[f_okreg]]['listaGmin'] = set()

    okregi[rekord[f_okreg]]['listaGmin'].add(rekord[f_gmina])

    #sumowanie dla gmin
    if rekord[f_gmina] in gminy:
        dodaj_do_sumy(gminy[rekord[f_gmina]], rekord)
    else:
        gminy[rekord[f_gmina]] = copy.deepcopy(rekord)

#kazdemu wojewodztwu sortuje listeOkregow
for x in wojewodztwa:
    wojewodztwa[x]['listaOkregow'] = sorted(wojewodztwa[x]['listaOkregow'])

print("googogoogogogoooogooogo")

template = env.get_template("index.html")

procentowyRozklad = copy.deepcopy(kandydaci)
for key, value in procentowyRozklad.items():
    procentowyRozklad[key] = round(value / lacznaSumaGlosow * 100, 5)

with open('strony/index.html', 'w') as out:
    out.write(template.render(
        _wojewodztwa=wojewodztwa,
        _kandydaci=procentowyRozklad,
        _wszystko=wszystko
    ))

template = env.get_template("wojewodztwo.html")
for x in wojewodztwa:
    nazwaPliku = 'strony/woj' + x +'.html'

    procentowyRozklad = copy.deepcopy(wojewodztwa[x]['kandydaci'])
    for key, value in procentowyRozklad.items():
        procentowyRozklad[key] = round(value / wojewodztwa[x][f_glosy_wazne] * 100, 5)

    with open(nazwaPliku, 'w') as out:
        out.write(template.render(
        _wojewodztwa=wojewodztwa,
        _wojewodztwo=wojewodztwa[x],
        _kandydaci=procentowyRozklad,
        _okregi=okregi
    ))

template = env.get_template("okreg.html")
for x in okregi:
    nazwaPliku = 'strony/okr' + str(x) +'.html'
    procentowyRozklad = copy.deepcopy(okregi[x]['kandydaci'])
    for key, value in procentowyRozklad.items():
        procentowyRozklad[key] = round(value / okregi[x][f_glosy_wazne] * 100, 5)

    with open(nazwaPliku, 'w') as out:
        out.write(template.render(
        _wojewodztwa=wojewodztwa,
        _okreg=okregi[x],
        _kandydaci=procentowyRozklad,
        _gminy=gminy
    ))

template = env.get_template("gmina.html")
for x in gminy:
    nazwaPliku = 'strony/gmi' + str(x) + '.html'
    procentowyRozklad = copy.deepcopy(gminy[x]['kandydaci'])
    for key, value in procentowyRozklad.items():
        procentowyRozklad[key] = round(value / gminy[x][f_glosy_wazne] * 100, 5)

    with open(nazwaPliku, 'w') as out:
        out.write(template.render(
            _wojewodztwa=wojewodztwa,
            _kandydaci=procentowyRozklad,
            _gminy=gminy,
            _gmina=gminy[x]
        ))

#pprint(okregi)
#pprint(gminy)